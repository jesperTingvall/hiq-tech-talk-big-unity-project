Code for HiQ Tech talk. Demonstration on techniques for building larger Unity projects and keeping code base clean.

The different stages of the project can be found as commits.

1. Adding shared code as a Unity package
2. Adding a Unity Package as a Git Submodule.
3. Seperation of code base into different name spaces and assembly definitions
4. Adding Unit tests and code coverage.
5. Adding assembly info files.
