using System.Reflection;

[assembly: AssemblyTitle("BigUnityProject.Components")]
[assembly: AssemblyDescription("Components")]
[assembly: AssemblyCompany("HiQ")]
[assembly: AssemblyProduct("BigUnityProject")]
[assembly: AssemblyCopyright("Copyright � 2021 - HiQ")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

