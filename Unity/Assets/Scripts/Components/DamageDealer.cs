using UnityEngine;

namespace HiQ.BigUnityProject.Components {
    [RequireComponent(typeof(HealthComponent))]
    public class DamageDealer : MonoBehaviour {

        private HealthComponent Health;

        private void Awake() {
            Health = GetComponent<HealthComponent>();
        }

        void OnMouseDown() {
            Health.Damage(1);
        }
    }
}
