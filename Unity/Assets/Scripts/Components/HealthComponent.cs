using HiQ.BigUnityProject.Model;
using UnityEngine;
using UnityEngine.Events;

namespace HiQ.BigUnityProject.Components {
    public class HealthComponent : MonoBehaviour {
        public UnityEvent<Health> OnHealthUpdated;

        public Health Health {
            private set;
            get;
        } = new Health(10);

        public void Damage(int damage) {
            Health.Damage(damage);
            OnHealthUpdated.Invoke(Health);
        }
    }
}
