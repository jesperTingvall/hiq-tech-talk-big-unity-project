using HiQ.BigUnityProject.Model;
using NUnit.Framework;
using UnityEngine;

namespace HiQ.BigUnityProject.Components.Tests {
    public class HealthComponentTests {
        private HealthComponent health;
        private GameObject enemy;

        [SetUp]
        public void Setup() {
            enemy = new GameObject();
            health = enemy.AddComponent<HealthComponent>();
        }

        [Test]
        public void HealthComponent_Damage_SendsOnHealthUpdated() {
            int updatedHealth = -999;
            health.OnHealthUpdated = new UnityEngine.Events.UnityEvent<Model.Health>();
            health.OnHealthUpdated.AddListener((Health x) => { updatedHealth = x.CurrentHealth; });

            health.Damage(1);

            Assert.AreEqual(9, updatedHealth);
        }


        [TearDown]
        public void Teardown() {
            Object.DestroyImmediate(enemy);
        }
    }
}
