using System.Reflection;

[assembly: AssemblyTitle("BigUnityProject.Model")]
[assembly: AssemblyDescription("Model")]
[assembly: AssemblyCompany("HiQ")]
[assembly: AssemblyProduct("BigUnityProject")]
[assembly: AssemblyCopyright("Copyright � 2021 - HiQ")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

