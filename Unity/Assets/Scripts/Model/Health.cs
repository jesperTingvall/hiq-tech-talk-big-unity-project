using System;

namespace HiQ.BigUnityProject.Model {
    public class Health {
        public int MaxHealth {
            get;
            set;
        }

        private int _currentHealth;
        public int CurrentHealth {
            get {
                return _currentHealth;
            }
            set {
                _currentHealth = Math.Max(value, 0);
                _currentHealth = Math.Min(_currentHealth, MaxHealth);
            }
        }

        public bool IsAlive {
            get {
                return CurrentHealth > 0;
            }
        }

        public Health(int health) {
            MaxHealth = health;
            CurrentHealth = health;
        }

        public void Damage(int damage) {
            CurrentHealth -= damage;
        }
    }
}
