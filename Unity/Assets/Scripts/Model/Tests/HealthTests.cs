using NUnit.Framework;

namespace HiQ.BigUnityProject.Model.Tests {
    public class ModelTests {
        private Health health;

        [SetUp]
        public void Setup() {
            health = new Health(10);

        }

        [Test]
        public void Health_PositiveHealth_IsAlive() {
            Assert.IsTrue(health.IsAlive);
        }

        [Test]
        public void Health_ZeroHealth_IsNotAlive() {
            health.CurrentHealth = 0;
            Assert.IsFalse(health.IsAlive);
        }

        [Test]
        public void Health_SetBelowZeroHealth_ClampsAtZero() {
            health.CurrentHealth = -54;
            Assert.AreEqual(0, health.CurrentHealth);
        }

        [Test]
        public void Health_SetAboveMaxHealth_ClampsToMaxHealth() {
            health.CurrentHealth = 54;
            Assert.AreEqual(10, health.CurrentHealth);
        }

        [Test]
        public void Health_Damage_ReducesCurrentHealth() {
            health.Damage(5);
            Assert.AreEqual(5, health.CurrentHealth);
        }
    }
}
