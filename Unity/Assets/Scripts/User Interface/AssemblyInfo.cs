using System.Reflection;
using UnityEngine.TestTools;

[assembly: AssemblyTitle("BigUnityProject.View")]
[assembly: AssemblyDescription("View")]
[assembly: AssemblyCompany("HiQ")]
[assembly: AssemblyProduct("BigUnityProject")]
[assembly: AssemblyCopyright("Copyright � 2021 - HiQ")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ExcludeFromCoverage]
