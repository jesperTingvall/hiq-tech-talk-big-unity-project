using HiQ.BigUnityProject.Components;
using HiQ.BigUnityProject.Model;
using UnityEngine;
using UnityEngine.UI;

namespace HiQ.BigUnityProject.UserInterface {
    [RequireComponent(typeof(Slider))]
    public class HealthBar : MonoBehaviour {
        [SerializeField]
        private HealthComponent HealthComponent;

        private Slider Slider;

        void Start() {
            Slider = GetComponent<Slider>();

            HealthComponent?.OnHealthUpdated.AddListener(HealthUpdated);
            HealthUpdated(HealthComponent.Health);
        }

        private void HealthUpdated(Health health) {
            Slider.maxValue = health.MaxHealth;
            Slider.value = health.CurrentHealth;
        }

        private void OnDestroy() {
            HealthComponent?.OnHealthUpdated.AddListener(HealthUpdated);
        }
    }
}
